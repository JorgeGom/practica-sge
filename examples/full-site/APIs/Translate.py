# -*- coding: utf-8 -*-
import requests

apiKey = "8e3a16f12dbd49b481927f02af13d9fb"

def translateToSpanish(text):   	
	token = getToken()
	url = "https://api.microsofttranslator.com/v2/http.svc/Translate?appid=Bearer%20" + token + "&text=" + text + "&to=es"

	r = requests.get(url)

	if not r.ok: # Returns status code
		return None

	textTranslate = r.content.replace('<string xmlns="http://schemas.microsoft.com/2003/10/Serialization/">', "").replace("</string>", "")

	return textTranslate

def getToken():
	url = "https://api.cognitive.microsoft.com/sts/v1.0/issueToken?Subscription-Key=" + apiKey
	r = requests.post(url)
	return r.content