import sqlalchemy, datetime
from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy import Boolean, DateTime, Text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker

import requests

Base = declarative_base()

class Tabla(Base):
    __tablename__ = "tabla"

    id = Column(Integer, primary_key=True)
    campo = Column(String(300), nullable=True)

class Sitio(Base):
    __tablename__ = "sitio"

    id = Column(Integer, primary_key=True)
    url = Column(String(512), nullable=True)
    description = Column(String(1024), nullable=True)
    visited = Column(Boolean, default=False)
    pending = Column(Boolean, default=False)
    created_at = Column(DateTime, nullable=False)
    updated_at = Column(DateTime, nullable=False)
    visited_at = Column(DateTime, nullable=True)

class ContenidoSitio(Base):
    __tablename__ = "contenido_sitio"

    id = Column(Integer, primary_key=True)
    sitio = relationship(Sitio)
    sitio_id = Column(
        Integer,
        ForeignKey('sitio.id'), 
        nullable=False
        )
    content = Column(Text, nullable=True)
    is_json = Column(Boolean, default=False)


class Database():
    engine = None
    session = None

    def __init__(self, databaseType = 'sqlite'):
        self.engine = self.start_engine(databaseType)
        self.init_database()

    def start_engine(self, databaseType):
        self.engine = create_engine(
            self.get_engine(databaseType)
            )
        return self.engine

    def get_engine(self, engineType):
        if engineType == 'sqlite':
            return 'sqlite:///database_test.db'
        elif engineType == 'postgres':
            return 'postgresql://%s:%s@%s/%s' % (
                'ubuntu',
                'ubuntu',
                'localhost',
                'flask'
                )
        else:
            return None

    def init_database(self):
        Base.metadata.create_all(self.engine)
        Base.metadata.bind = self.engine
        DBSession = sessionmaker(bind=self.engine)
        self.session = DBSession()
        return True

    def get_entries(self):
        s = self.session.query(Tabla)
        return s.all()

    def add_entries(self, text=None):
        new_entry = Tabla(
            campo = text
            )
        self.session.add(new_entry)
        self.session.commit()
        self.session.close()
        return new_entry

    def get_single_entry(self, entryId):
        s = self.session.query(Tabla).filter(Tabla.id == entryId)
        try:
            return s.one()
        except Exception as e:
            return None

    def update_entry(self, entry, campo = None):
        entry.campo = campo
        self.session.add(entry)
        self.session.commit()
        self.session.close()
        return entry

    def delete_entry(self, entry):
        self.session.delete(entry)
        self.session.commit()
        self.session.close()
        return True

    #Devuelve un listado de todos los sitios
    def get_sitios(self):
        s = self.session.query(Sitio)
        return s.all()

    #Devuelve un listado de sitios visitados/no visitados
    def get_sitios_visitados(self, visitados = True):
        #El argumento visitados cambiara el tipo de filtro
        s = self.session.query(Sitio).filter(
            Sitio.visited == visitados,
            Sitio.pending == False
            )
        return s.all()

    #Devuelve un listado de sitios pendientes de visitar
    def get_sitios_pendientes(self):
        s = self.session.query(Sitio).filter(
            Sitio.visitado == False,
            Sitio.pending == True
            )
        return s.all()

    #Hace un sitio pendiente de visitar
    def make_sitio_pendiente(self, sitio):
        try:
            sitio.pending = True
            sitio.updated_at = datetime.datetime.now()
            self.session.add(sitio);self.session.commit()
            self.session.close()
            return True
        except Exception as e:
            return False

    #Visitar un sitio
    def make_sitio_visitado(self, sitio):
        try:
            sitio.visited = True
            sitio.visited_at = datetime.datetime.now()
            sitio.updated_at = datetime.datetime.now()
            self.session.add(sitio);self.session.commit()
            self.session.close()
            return True
        except Exception as e:
            return False

    #Crea un sitio
    def create_sitio(self, url, description = None):
        new_sitio = Sitio(
            url = url,
            description = description,
            created_at = datetime.datetime.now(),
            updated_at = datetime.datetime.now()
            )
        self.session.add(new_sitio)
        self.session.commit()
        return new_sitio

    #Lista contenido de todos los sitios
    def get_contenidos(self):
        c = self.session.query(ContenidoSitio)
        return c.all()

    #Lista contenido json/no json de todos los sitios
    def get_contenidos_json(self, is_json = True):
        c = self.session.query(ContenidoSitio).filter(
            ContenidoSitio.is_json == is_json
            )
        return c.all()

    #Lista contenido de un sitio
    def get_contenidos_sitio(self, sitio):
        c = self.session.query(ContenidoSitio).filter(
            ContenidoSitio.sitio == sitio
            )
        return c.all()

    #Lista contenido json/no json de un sitio
    def get_contenidos_json_sitio(self, sitio, is_json = True):
        c = self.session.query(ContenidoSitio).filter(
            ContenidoSitio.sitio == sitio,
            ContenidoSitio.is_json == is_json
            )
        return c.all()

    #Get sitio por ID
    def get_sitio_by_id(self, sitio_id):
        try:
            s = self.session.query(Sitio).filter(
                Sitio.id == sitio_id
                )
            return s.one()
        except Exception as e:
            return None

    #Get sitios por url
    def get_sitios_by_url(self, url):
        if type(url) not in [str,unicode]:
            return []
        s = self.session.query(Sitio).filter(
            Sitio.url.like('%'+url+'%')
            )
        return s.all()

    #Crea un contenido de sitio
    def create_contenido_sitio(self, sitio, contenido, is_json = False):
        new_contenido = ContenidoSitio(
            sitio = sitio,
            content = contenido,
            is_json = is_json
            )
        self.session.add(new_contenido)
        self.session.commit()
        return new_contenido

    #Actualizar cualquier entidad
    #ejemplo: d.update_entity(sitio, url='google.es', pending=True)
    def update_entity(self, entity, **campos):
        try:
            for x in campos:
                if hasattr(entity, x):
                    setattr(entity, x, campos[x])
            self.session.add(entity)
            self.session.commit()
            self.session.close()
            return True
        except Exception as e:
            return False


print("Call to the database to test")

from flask import Flask, request, render_template, \
    redirect

app = Flask(__name__)

d = Database()
d1 =  Database('postgres')

@app.route('/')
def home():
    elements = d.get_entries()
    context = {
        'elements' : elements
    }
    return render_template('hola.html', content=context)

@app.route('/post', methods=['POST'])
def post():
    if request.method == 'POST':
        d.add_entries(request.form['campo'])
        d1.add_entries(request.form['campo'])
    return redirect("/")

@app.route('/database/<dataType>')
def databasesList(dataType):
    context = {}
    if dataType == 'sqlite':
        context['elements'] = d.get_entries()
    elif dataType == 'postgres':
        context['elements'] = d1.get_entries()
    context['database'] = dataType
    return render_template('hola.html', content=context)

@app.route('/update/<database>/<int:entryId>', methods=['GET','POST'])
def updateEntry(database, entryId):
    e = None
    dengine = None
    if database == 'sqlite':
        e = d.get_single_entry(entryId)
        dengine = d
    elif database == 'postgres':
        e = d.get_single_entry(entryId)
        dengine = d
    if request.method == 'GET':
        return render_template(
            'formulario.html', 
            database = database, 
            entrada = e )
    elif request.method == 'POST' and e:        
        dengine.update_entry(e, request.form['campo'])
    return redirect('/database/%s' % database)

@app.route('/delete/<database>/<int:entryId>', methods=['POST'])
def deleteEntry(database, entryId):
    e = None
    dengine = None
    if database == 'sqlite':
        e = d.get_single_entry(entryId)
        dengine = d
    elif database == 'postgres':
        e = d.get_single_entry(entryId)
        dengine = d
    if e:
        dengine.delete_entry(e)
    return redirect('/database/%s' % database)

@app.route('/search/page', methods=['GET','POST'])
def searchPage():
    if request.method == 'GET':
        context = {
            'request' : None
        }
    elif request.method == 'POST':
        url = request.form['url']
        try:
            content = requests.get(url)
            content = content.text
        except Exception as e:
            content = "<h1 style='color:red;'> SIN RESULTADOS</h1>"
        context = {
            'request': content
        }
    return render_template('busqueda.html', context=context)

@app.route('/download/links', methods=['GET','POST'])
def downloadLinks():
    if request.method == 'GET':
        context = {
            'request': None
        }
    elif request.method == 'POST':
        url = request.form['url']
        content = []
        from web_spider import LinkDownloader
        import traceback
        try:
            d = LinkDownloader('http://www.allitebooks.com')
            #content = d.start_spider()
            content = d.itebooks()
            del d
        except Exception as e:
            print traceback.format_exc()
            print(u"No hay enlaces")
        context = {
            'request': content
        }
    return render_template('download.html', context=context)

@app.route('/download/books', methods=['GET'])
def downloadBooks():
    from download_links import download_book_from_itebooks
    print("downloading books")
    download_book_from_itebooks('data.json')
    return "Descargados"

if __name__ == '__main__':
    app.run(debug=True)