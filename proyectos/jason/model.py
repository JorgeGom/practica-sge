# -*- coding: utf-8 -*-
import binascii
import os

import sqlalchemy, datetime

from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy import Boolean, DateTime, Text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker

from settings import Settings
from werkzeug.security import check_password_hash, generate_password_hash

Base = declarative_base()


def generate_random_token(value=32):
    return binascii.b2a_hex(os.urandom(value))


def get_password_hash(password):
    return generate_password_hash(password)


def check_password(password_hash, password):
    return check_password_hash(password_hash, password)


class User(Base):
    __tablename__ = "user"

    id = Column(Integer, primary_key=True)
    email = Column(String(200), nullable=False, unique=True)
    username = Column(String(160), nullable=False, unique=True)
    first_name = Column(String(200), nullable=True)
    last_name = Column(String(200), nullable=True)
    password = Column(String(1024), nullable=False)
    user_hash = Column(String(2048), nullable=True)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    deleted_at = Column(DateTime, nullable=True)
    active = Column(Boolean, default=False)
    activation_token = Column(String(512), nullable=True, unique=True)
    is_admin = Column(Boolean, default=False)
    is_superadmin = Column(Boolean, default=False)


class UserSession(Base):
    __tablename__ = "user_session"

    id = Column(Integer, primary_key=True)
    session_token = Column(String(2048), nullable=False)
    user = relationship(User)
    user_id = Column(Integer, ForeignKey('user.id'), nullable=True)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    deleted_at = Column(DateTime, nullable=True)
    active = Column(Boolean, default=True)


class Page(Base):
    __tablename__ = "page"

    id = Column(Integer, primary_key=True)
    title = Column(String(256), nullable=False)
    slug = Column(String(256), nullable=False, unique=True)
    content = Column(Text, nullable=False)
    seo_tags = Column(String(512))
    seo_description = Column(String(1024))
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    deleted_at = Column(DateTime, nullable=True)
    active = Column(Boolean, default=True)


class Category(Base):
    __tablename__ = "category"

    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False, unique=True)
    slug = Column(String(255), nullable=False, unique=True)
    description = Column(String(1024), nullable=True)
    keywords = Column(String(512), nullable=True)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    deleted_at = Column(DateTime, nullable=True)
    active = Column(Boolean, default=True)


class Tags(Base):
    __tablename__ = "tags"

    id = Column(Integer, primary_key=True)
    name = Column(String(255), nullable=False)
    created_at = Column(DateTime)


class Entry(Base):
    __tablename__ = "entry"

    id = Column(Integer, primary_key=True)
    title = Column(String(255), nullable=False)
    slug = Column(String(255), nullable=False)
    content = Column(Text, nullable=True)
    description = Column(String(512), nullable=True)
    keywords = Column(String(512), nullable=True)
    category = relationship(Category)
    category_id = Column(ForeignKey('category.id'), nullable=False)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    deleted_at = Column(DateTime, nullable=True)
    published_at = Column(DateTime)
    active = Column(Boolean, default=True)
    draft = Column(Boolean, default=False)


class EntryTags(Base):
    __tablename__ = "entry_tags"

    id = Column(Integer, primary_key=True)
    entry = relationship(Entry)
    tag = relationship(Tags)
    entry_id = Column(ForeignKey('entry.id'))
    tag_id = Column(ForeignKey('tags.id'))


class Comment(Base):
    __tablename__ = "comment"

    id = Column(Integer, primary_key=True)
    content = Column(String(2500), nullable=False)
    user = relationship(User)
    user_id = Column(ForeignKey('user.id'))
    created_at = Column(DateTime)
    updated_at = Column(DateTime)
    deleted_at = Column(DateTime, nullable=True)
    published_at = Column(DateTime)
    active = Column(Boolean, default=True)
    reported = Column(Boolean, default=False)
    entry = relationship(Entry)
    entry_id = Column(ForeignKey('entry.id'))


class Model:
    engine = None
    engine_type = None
    filename = None
    session = None
    tables = {}

    def __init__(self, engine_type, filename=None, debug=False):
        self.engine_type = engine_type
        if engine_type not in ['mysql', 'postgres']:
            self.filename = filename
            if filename in [None, '']:
                raise Exception("Si no es MySQL o SQLITE\
                    se debe incluir nombre del fichero")
        self.start_engine(debug)
        self.init_database()
        self.create_basic_users()

    def start_engine(self, debug=False):
        if self.engine_type:
            self.engine = create_engine(
                self.get_engine(self.engine_type),
                echo=debug
            )
            return True
        else:
            return False

    def get_engine(self, engine_type):
        if engine_type == 'mysql':
            engine = 'mysql://%s:%s@%s/%s' % (
                Settings['DB_USER'],
                Settings['DB_PASS'],
                Settings['DB_HOST'],
                Settings['DB_NAME']
            )

        else:
            engine = 'sqlite:///%s.db' % self.filename
        return engine

    def init_database(self):
        Base.metadata.create_all(self.engine)
        Base.metadata.bind = self.engine
        dbSession = sessionmaker(bind=self.engine)
        self.session = dbSession()
        return True

    def delete_sqlalchemy_entity(self, entity):
        self.session.delete(entity)
        self.session.commit()
        self.session.close()
        return True

    def update_sqlalchemy_entity(self, entity, **fields):
        for x in fields:
            try:
                if hasattr(entity, x):
                    setattr(entity, x, fields[x])
            except Exception as e:
                print(e)
                pass
        self.session.add(entity)
        self.session.commit()
        return entity

    def create_sqlalchemy_entity(self, entity, **fields):
        e = entity()
        for x in fields:
            try:
                if hasattr(e, x):
                    setattr(e, x, fields[x])
            except Exception as e:
                print(e)
                pass
        self.session.add(e)
        self.session.commit()
        return e

    def get_user_by_id(self, user_id):
        try:
            u = self.session.query(User).filter(User.id == int(user_id))
            return u.one()
        except Exception as e:
            print(e)
            return None

    def get_users(self, email):
        u = self.session.query(User).filter(User.email.like('%' + email + '%')).all()
        return u

    def activate_user(self, activation_token):
        u = self.session.query(User).filter(User.activation_token == activation_token)
        try:
            u = u.one()
            u.active = True
            u.activation_token = None
            self.session.add(u)
            self.session.commit()
            self.session.close()
            return True
        except Exception as e:
            print(e)
            return False

    def create_user(self, email, username, password, admin=False):
        hash_user = get_password_hash(password)
        user = User(
            email=email,
            username=username,
            password=hash_user,
            created_at=datetime.datetime.now(),
            updated_at=datetime.datetime.now(),
            activation_token=generate_random_token(),
            is_admin=admin,
            is_superadmin=admin
        )
        self.session.add(user)
        self.session.commit()
        return user

    def create_basic_users(self):
        u = self.get_users('jasonjimenezcruz@gmail.com')
        if len(u) == 0:
            user = self.create_user(
                'jasonjimenezcruz@gmail.com',
                'jason',
                'jason123',
                True
            )
            print(user.email)
            self.session.close()
        return True

    def update_user(self, user, **fields):
        for x in fields:
            try:
                if fields[x] != 'password':
                    setattr(user, x, fields[x])
                else:
                    setattr(user, x, get_password_hash(fields[x]))
            except Exception as e:
                print(e)
                pass
        user.updated_at = datetime.datetime.now()
        self.session.add(user)
        self.session.commit()
        self.session.close()
        return user

    def get_user_by_email(self, email):
        u = self.session.query(User).filter(User.email == email)
        try:
            return u.one()
        except Exception as e:
            print(e)
            return None

    def create_user_session(self, user):
        user_session = UserSession(
            session_token=generate_random_token(1024),
            user=user,
            created_at=datetime.datetime.now(),
            updated_at=datetime.datetime.now()
        )
        token = user_session.session_token
        self.session.add(user_session)
        self.session.commit()
        self.session.close()
        return token

    def get_user_session_by_token(self, token):
        try:
            s = self.session.query(UserSession).filter(UserSession.session_token == token,
                                                       UserSession.active == True).one()
        except Exception as e:
            print(e)
            s = None

        return s

    def get_user_sessions_by_user(self, email):
        u = self.get_user_by_email(email)
        s = self.session.query(UserSession).filter(UserSession.user == u, UserSession.active == True)
        return s.all()

    def create_page(self, title, slug, content, tags = None, description = None):
        new_page = Page(
            title = title,
            slug = slug,
            content = content,
            seo_tags = tags,
            seo_description = description,
            active = True,
            created_at = datetime.datetime.now(),
            updated_at = datetime.datetime.now()
        )
        self.session.add(new_page)
        self.session.commit()
        return new_page

    def find_page_by_id(self, page_id):
        p = self.session.query(Page).filter(
            Page.id == page_id
        )
        return p.one()

    def find_page_by_slug(self, page_slug):
        p = self.session.query(Page).filter(
            Page.slug == page_slug
        )
        return p.one()

    def find_pages_by_title(self, title):
        p = self.session.query(Page).filter(
            Page.title.like('%'+title+'%'),
            Page.active == True
        )
        return p.all()

    def get_categories(self):
        c = self.session.query(Category).filter(
            Category.active == True
            )
        return c.all()

    def create_category(self, name, slug, description = None, keywords = None):
        new_category = Category(
            name = name,
            slug = slug,
            description = description,
            keywords = keywords,
            active=True,
            created_at=datetime.datetime.now(),
            updated_at=datetime.datetime.now()
        )
        self.session.add(new_category)
        self.session.commit()
        return new_category

    def find_category_by_id(self, category_id):
        c = self.session.query(Category).filter(
            Category.id == category_id,
            Category.active == True
        )
        return c.one()

    def find_category_by_slug(self, category_slug):
        c = self.session.query(Category).filter(
            Category.slug == category_slug,
            Category.active == True
        )
        return c.one()

    def find_categories_by_name(self, name):
        c = self.session.query(Category).filter(
            Category.name.like('%'+name+'%')
        )
        return c.all()

    def create_entry(self, title, slug, content,category, draft, description = None, keywords = None):
        new_entry = Entry(
            title = title,
            slug = slug,
            content = content,
            category = category,
            draft = draft,
            description = description,
            keywords = keywords,
            active = True,
            created_at = datetime.datetime.now(),
            updated_at = datetime.datetime.now()
        )
        self.session.add(new_entry)
        self.session.commit()
        return new_entry

    def find_entry_by_id(self, entry_id):
        e = self.session.query(Entry).filter(
            Entry.id == entry_id,
            Entry.active == True
        )
        return e.one()

    def find_entry_by_slug(self, entry_slug):
        e = self.session.query(Entry).filter(
            Entry.slug == entry_slug,
            Entry.active == True
        )
        return e.one()

    def find_entries_by_title(self, title):
        e = self.session.query(Entry).filter(
            Entry.title.like('%'+title+'%'),
            Entry.active == True
        )
        return e.all()

    def find_entries_by_content(self, content):
        e = self.session.query(Entry).filter(
            Entry.content.like('%'+content+'%'),
            Entry.active == True
        )
        return e.all()

    def find_entries_by_category(self, category):
        e = self.session.query(Entry).filter(
            Entry.category == category,
            Entry.active == True
        )
        return e.all()

    def create_tag(self, name):
        new_tag = Tags(
            name = name,
            created_at = datetime.datetime.now()
        )
        self.session.add(new_tag)
        self.session.commit()
        return new_tag

    def find_tag_by_id(self, tag_id):
        t = self.session.query(Tags).filter(
            Tags.id == tag_id
        )
        return t.one()

    def find_tag_by_name(self, name):
        t = self.session.query(Tags).filter(
            Tags.name == name
        )
        return t.one()

    def seach_tags(self, query):
        t = self.session.query(Tags).filter(
            Tags.name.like('%'+query+'%')
        )
        return t.one()

    def create_entry_tag(self, entry, tag):
        new_entry_tag = EntryTags(
            entry = entry,
            tag = tag
        )
        self.session.add(new_entry_tag)
        self.session.commit()
        return new_entry_tag

    def find_tags_by_entry(self, entry):
        et = self.session.query(EntryTags).filter(
            EntryTags.entry == entry
        )
        return et.all()

    def find_entries_by_tag(self, tag):
        et = self.session.query(EntryTags).filter(
            EntryTags.tag == tag
        )
        return et.all()

    def create_comment(self, user, entry, content):
        new_comment = Comment(
            user = user,
            entry = entry,
            content = content
        )
        self.session.add(new_comment)
        self.session.commit()
        return new_comment

    def find_comment_by_id(self, comment_id):
        c = self.session.query(Comment).filter(
            Comment.id == comment_id
        )
        return c.one()

    def find_comments_by_user(self, user):
        c = self.session.query(Comment).filter(
            Comment.user == user
        )
        return c.all()

    def find_comments_by_entry(self, entry):
        c = self.session.query(Comment).filter(
            Comment.entry == entry
        )
        return c.all()