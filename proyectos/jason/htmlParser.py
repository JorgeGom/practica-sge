# -*- coding: utf-8 -*-
import requests, pdb, traceback, os, json, time
from multiprocessing.pool import ThreadPool
from bs4 import BeautifulSoup

class Parser():
    obj = None
    data = None

    def __init__(self, data):
        self.data = data
        self.parseHtml(data)

    def parseHtml(self, htmlText):
        self.obj = BeautifulSoup(htmlText, 'html.parser')
        return self.obj

    def findElements(self, tagElement, **arguments):
        return self.obj.find_all(tagElement, **arguments)